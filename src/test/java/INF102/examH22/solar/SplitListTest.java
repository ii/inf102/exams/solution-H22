package INF102.examH22.solar;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class SplitListTest {

	List<Integer> input;
	SplitList split;

	@BeforeEach
	void init(){
		input = of(2,6,3,5,7,2,5);
		split = new SplitList(input);		
	}

	@Test
	void testSplitList() {
		assertEquals(2, split.getCurrent());
		assertEquals(6, split.numAbove());
		assertEquals(28, split.sumAbove());
	}

	@Test
	void testSumBelow() {
		assertEquals(0, split.sumBelow());
		split.next();
		assertEquals(2, split.sumBelow());
		split.next();
		assertEquals(4, split.sumBelow());
		split.next();
		assertEquals(7, split.sumBelow());
		split.next();
		assertEquals(12, split.sumBelow());
		split.next();
		assertEquals(17, split.sumBelow());
		split.next();
		assertEquals(23, split.sumBelow());
	}

	@Test
	void testSumAbove() {
		assertEquals(28, split.sumAbove());
		split.next();
		assertEquals(26, split.sumAbove());
		split.next();
		assertEquals(23, split.sumAbove());
		split.next();
		assertEquals(18, split.sumAbove());
		split.next();
		assertEquals(13, split.sumAbove());
		split.next();
		assertEquals(7, split.sumAbove());
		split.next();
		assertEquals(0, split.sumAbove());	}

	@Test
	void testNumBelow() {
		assertEquals(0, split.numBelow());
		split.next();
		assertEquals(1, split.numBelow());
		split.next();
		assertEquals(2, split.numBelow());
		split.next();
		assertEquals(3, split.numBelow());
		split.next();
		assertEquals(4, split.numBelow());
		split.next();
		assertEquals(5, split.numBelow());
		split.next();
		assertEquals(6, split.numBelow());
	}

	@Test
	void testNumAbove() {
		assertEquals(6, split.numAbove());
		split.next();
		assertEquals(5, split.numAbove());
		split.next();
		assertEquals(4, split.numAbove());
		split.next();
		assertEquals(3, split.numAbove());
		split.next();
		assertEquals(2, split.numAbove());
		split.next();
		assertEquals(1, split.numAbove());
		split.next();
		assertEquals(0, split.numAbove());
	}

	@Test
	void testGetCurrent() {
		int cur = split.getCurrent();
		while(split.hasNext()) {
			int next = split.next();
			assertTrue(cur<=next);
			cur = next;
		}
	}
	
	@Test
	void testIterate() {
		ArrayList<Integer> output = new ArrayList<>();
		output.add(split.getCurrent());
		while(split.hasNext()) 
			output.add(split.next());
		
		for(int i : input)
			assertEquals(Collections.frequency(input, i), Collections.frequency(output, i)
					,"a number in input was not iterated over the right number of times");
		
		for(int i : output)
			assertEquals(Collections.frequency(input, i), Collections.frequency(output, i)
					,"a number in output was not iterated over the right number of times");
	}
	
	@Test
	void testAdd() {
		assertEquals(2, split.getCurrent());
		int numAbove = split.numAbove();
		split.add(split.getCurrent()+10);
		assertEquals(numAbove+1, split.numAbove());
		assertEquals(2, split.getCurrent());
		split.next();
		assertEquals(2, split.getCurrent());
		split.next();
		assertEquals(3, split.getCurrent());
		split.next();
		assertEquals(5, split.getCurrent());
		assertEquals(30,split.sumAbove());
		int numBelow = split.numBelow();
		split.add(split.getCurrent()-10);
		assertEquals(numBelow+1, split.numBelow());		
	}

	@Test
	void testRemoveSmallest() {
		assertEquals(2, split.removeSmallest());
		assertEquals(2, split.getCurrent());
		assertEquals(2, split.removeSmallest());
		assertEquals(3, split.getCurrent());
		split.next();
		split.next();
		split.next();
		assertEquals(6, split.getCurrent());
		assertEquals(3, split.removeSmallest());
	}
	
	private List<Integer> of(int ...ints) {
		List<Integer> list = new ArrayList<>();
		for(int i : ints)
			list.add(i);
		return list;
	}
}
