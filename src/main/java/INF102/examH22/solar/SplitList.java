package INF102.examH22.solar;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * This class represents a list split in 2 parts, the low numbers and the high numbers.
 * When a new instance is created all numbers should be in the hi part.
 * The class implements the interface Iterator every time next() is called
 * one number will move from the high part to the low part.
 * The next number in the iterator defines the split point, 
 * i.e. all numbers in low are <= next() and all numbers in hi are >= next() 
 * 
 * @author Martin Vatshelle
 *
 */
public class SplitList implements Iterator<Integer> {

	private PriorityQueue<Integer> low; //the low numbers
	private PriorityQueue<Integer> hi; //the high numbers
	private Integer current;
	//store sums of hi and low so they do not have to be recomputed
	int sumHi;
	int sumLow;
	
	/**
	 * Constructs a SplitList where all elements are in hi
	 * list can not be empty, at least 1 element needs to be provided
	 * @param list The list of numbers this class should keep track of
	 */
	public SplitList(List<Integer> list) { 
		//Since removeMin takes O(n) in ArrayList we change to PriorityQueue
		low = new PriorityQueue<>();
		sumLow = 0;
		hi = new PriorityQueue<>(list);
		current = removeMin(hi);
		sumHi = sum(hi); //O(n)
	}

	//removes and returns the smallest element from hi
	//this changes now so it only works on PriorityQueue
	private int removeMin(PriorityQueue<Integer> c) {  
		return c.remove(); 
	}
	
	//returns the sum of all elements in low
	public int sumBelow() {
		return sumLow; //return sum in O(1)
	}

	//returns the sum of all elements in hi
	public int sumAbove() {
		return sumHi; //return sum in O(1)
	}
	
	//returns the sum of all elements in the given collection
	private int sum(Collection<Integer> coll) {
		int sum = 0;
		for(int val : coll)
			sum += val;
		return sum;
	}
	
	//returns the number of elements in low
	public int numBelow() {
		return low.size();
	}

	//returns the number of elements in hi
	public int numAbove() { 
		return hi.size();
	}
	
	//returns the current element
	public int getCurrent() {
		return current;
	}

	@Override
	public boolean hasNext() { 
		return !hi.isEmpty();
	}

	//next() changes current to the smallest element in hi and returns the new current
	@Override
	public Integer next() {
		low.add(current);
		sumLow += current; //update sum
		current = removeMin(hi);
		sumHi -= current; //update sum
		return current;
	}
	
	/**
	 * Adds a number to this SplitList. Current should not change so
	 * if num is smaller than current it needs to be added to low
	 * if num is >= current it will be added to hi.
	 * @param num the number to add
	 */
	public void add(int num) {
		if(num>=current) {
			hi.add(num);
			sumHi += num; //update sum
		}
		else {
			low.add(num);
			sumLow += num; //update sum
		}
	}

	/**
	 * This method removes the smallest element in this SplitList
	 * @return the element that was removed
	 */
	public int removeSmallest() {
		if(low.isEmpty()) {
			int smallest = current;
			current = removeMin(hi);
			sumHi -= current; //update sum
			return smallest;
		}
		int smallest = removeMin(low);
		sumLow -= smallest; //update sum
		return smallest;
	}
}
