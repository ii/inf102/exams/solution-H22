package INF102.examH22.solar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SolarLane implements ISolarLane {

    private Random random = new Random();

	@Override
    public Integer place1Robot(List<Integer> jobs) {
        return median(jobs);
    }

    @Override
    public Pair<Integer> place2Robots(List<Integer> jobs) {
		//create 2 SplitLists, one containing only the smallest element, one containing all other elements
		SplitList second = new SplitList(jobs);
		balance(second);
		ArrayList<Integer> f = new ArrayList<Integer>();
		f.add(second.removeSmallest());
		SplitList first=new SplitList(f);
		balance(second);
		//compute initial distance
		int bestDist = dist(first)+dist(second);
		Pair<Integer> bestPos = new Pair<Integer>(first.getCurrent(),second.getCurrent());
		//try all possible ways of dividing jobs between the two robots
		while(second.numAbove()>0) {
			first.add(second.removeSmallest());
			balance(first);
			balance(second);

			int dist = dist(first)+dist(second);
			if(dist<bestDist) {
				bestDist = dist;
				bestPos = new Pair<Integer>(first.getCurrent(),second.getCurrent());
			}
		}
		return bestPos;
	}

    //Since we know the best location for a robot is the middle element
    //this method makes sure current is the middle element of the SplitList
	private void balance(SplitList sl) {
		while(sl.numAbove()>sl.numBelow()) {
			sl.next();
		}
	}
 
    //this method computes the sum of distances a robot needs to travel
    //if the robot is places at current position and split is all jobs this robot should do
	private int dist(SplitList split) {
		int sumBelow = split.getCurrent()*split.numBelow()-split.sumBelow();
		int sumAbove = split.sumAbove()-split.getCurrent()*split.numAbove();
		return sumAbove+sumBelow;
	}

    
	public <T extends Comparable<? super T>> T median(List<T> list) {

		return quickSelect(list,list.size()/2);
	}

	private <T extends Comparable<? super T>> T quickSelect(List<T> list, int index) {
		
		T pivot = list.get(random.nextInt(list.size()));	
		List<T> smaller = new ArrayList<T>(list.size());
		List<T> larger = new ArrayList<T>(list.size());
		int numequal = 0;

		for(T t : list) { //n iterations //O(n)
			if(t.compareTo(pivot)<0) { 		//O(1)
				smaller.add(t); 			//O(1)
			}
			else {
				if(t.compareTo(pivot)>0) {	//O(1)
					larger.add(t);			//O(1)
				}
				else {
					numequal++;			//O(1)
				}
			}
		}
		if(index < smaller.size()) {
			return quickSelect(smaller, index);
		}
		else {
			if(index < smaller.size()+numequal) {
				return pivot;
			}
			else {
				return quickSelect(larger, index-smaller.size()-numequal);
			}
		}
	}
}
