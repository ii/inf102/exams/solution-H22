package INF102.examH22.solar;

import java.util.Collections;
import java.util.List;

public class SolarLane2 implements ISolarLane {

	@Override
    public Integer place1Robot(List<Integer> jobs) { //O(n log n)
        SplitList sl = new SplitList(jobs);
        int bestPos = sl.getCurrent();
        int bestDist = dist(sl);
        
        while(sl.hasNext()) {
        	sl.next();
        	if(dist(sl)<bestDist) {
        		bestDist = dist(sl);
        		bestPos = sl.getCurrent();
        	}
        }
        return bestPos;
    }

    @Override
    public Pair<Integer> place2Robots(List<Integer> jobs) { //O(n^2)
		Collections.sort(jobs); //after sorting we can compute median in O(1) time
		Pair<Integer> bestPos = null;
		int bestDist = Integer.MAX_VALUE;
		for(int i=1; i<jobs.size()-1; i++) {
			int pos1 = jobs.get(i/2); //first robot gets i jobs
			int pos2 = jobs.get((jobs.size()+i)/2);
			Pair<Integer> pos = new Pair<Integer>(pos1,pos2);
			if(dist(pos,jobs)<bestDist) {
				bestPos = pos;
				bestDist = dist(pos,jobs); 
			}
		}
		return bestPos;
	}

    private int dist(Pair<Integer> pos, List<Integer> jobs) { //O(n)
    	int sum = 0;
    	for(int job : jobs) {
    		int d1 = Math.abs(pos.loc1-job);
    		int d2 = Math.abs(pos.loc2-job);
    		sum += Math.min(d1, d2);
    	}
		return sum;
	}

	//this method computes the sum of distances a robot needs to travel
    //if the robot is places at current position and split is all jobs this robot should do
	private int dist(SplitList split) {
		int sumBelow = split.getCurrent()*split.numBelow()-split.sumBelow();
		int sumAbove = split.sumAbove()-split.getCurrent()*split.numAbove();
		return sumAbove+sumBelow;
	}
}